# Code Reutilization Example

Project Organization
------------

    ├── scrappers                       <- the different scrapper example will be here
    │         ├── gateways.py           <- This have the gateways and its used on the last example
    │         ├── scrapper_function.py  <- The second example turning the script into a function
    │         ├── scrapper_script.py    <- The first example, making a simple script
    │         └── scrapper_service.py   <- The final example, turning the function into a class
    ├── .env.tempalte                   <- Template of the environment variables
    ├── .gitignore                      <- Files to ingoner in the repository upload
    ├── Pipfile                         <- Requirements of the proyect
    ├── Pipfile.lock                    <- Requirements of the proyect
    └── README.md                       <- Description of the proyect

--------

## Environment instalation
```shell
$ pipenv install --dev
```

## Environment variables
Environment variable | Example value | Required | Default
--- | --- | --- | ---
PYTHONPATH  | ${PYTHONPATH}:src | YES | ${PYTHONPATH}:scrappers
