from pprint import PrettyPrinter
from typing import Dict

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def get_python_argentina_social_links() -> Dict:
    """Get the social networks links from the Pyhton Argentina
    Comunity

    Returns:
        Dict: with the name of the social network as the key and the link as the value
    """
    options = webdriver.ChromeOptions()
    options.add_argument("--disable-extensions")
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)

    driver.get(url="https://wiki.python.org.ar/foro_y_redes/")

    # Get links
    youtube = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "YouTube")]/following-sibling::a'
    ).text
    telegram = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Telegram")]/following-sibling::a'
    ).text
    twitter = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Twitter")]/following-sibling::a'
    ).text
    facebook = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Facebook")]/following-sibling::a'
    ).text
    instagram = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Instagram")]/following-sibling::a'
    ).text
    linkedin = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "LinkedIn")]/following-sibling::a'
    ).text

    return dict(
        youtube=youtube,
        telegram=telegram,
        twitter=twitter,
        facebook=facebook,
        instagram=instagram,
        linkedin=linkedin,
    )


if __name__ == "__main__":
    media_links = get_python_argentina_social_links()
    pp = PrettyPrinter(indent=4)
    pp.pprint(media_links)
