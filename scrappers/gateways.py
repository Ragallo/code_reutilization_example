from typing import Dict

from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from webdriver_manager.chrome import ChromeDriverManager


class ChromeGateway:
    _connected = False

    def get(self, url: str, *args, **kwargs) -> None:
        """
        A HTTP get requests
        """
        self._connect()
        self._driver.get(url, *args, **kwargs)

    def post(self, url: str, data: Dict, *args, **kwargs) -> None:
        """
        A HTTP post requests
        """
        self._connect()
        self._driver.post(url, data=data, *args, **kwargs)

    def find_element_by_xpath(self, xpath: str) -> WebElement:
        """Finds element for the curent web by xpath.
        Firs you need to connect and get a web to use this function

        Args:
            xpath (str): xpath of the element you want

        Returns:
            WebElement: the web element you need
        """
        return self._driver.find_element(By.XPATH, xpath)

    def _connect(self):
        if not self._connected:
            options = webdriver.ChromeOptions()
            options.add_argument("--disable-extensions")
            options.add_argument("--headless")
            options.add_argument("--disable-gpu")
            options.add_argument("--no-sandbox")
            self._driver = webdriver.Chrome(
                ChromeDriverManager().install(), chrome_options=options
            )


class ChromeHubGateway:
    def get(self, url: str, *args, **kwargs) -> None:
        """
        A HTTP get requests
        """
        self._connect()
        self.driver.get(url, *args, **kwargs)

    def post(self, url: str, data: Dict, *args, **kwargs) -> None:
        """
        A HTTP post requests
        """
        self._connect()
        self.driver.post(url, data=data, *args, **kwargs)

    def find_element_by_xpath(self, xpath: str) -> WebElement:
        """Finds element for the curent web by xpath.
        Firs you need to connect and get a web to use this function

        Args:
            xpath (str): xpath of the element you want

        Returns:
            WebElement: the web element you need
        """
        return self._driver.find_element(By.XPATH, xpath)

    def _connect(self):
        self.driver = webdriver.Remote(
            "http://chrome:4444/wd/hub", DesiredCapabilities.CHROME
        )
