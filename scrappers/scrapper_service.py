from enum import Enum
from pprint import PrettyPrinter
from typing import Dict, Union

from scrappers.gateways import ChromeGateway, ChromeHubGateway


class PythonArgWebEnums(Enum):
    social_media_url = "https://wiki.python.org.ar/foro_y_redes/"
    social_media_youtube_xpath = (
        '//p/strong[contains(text(), "YouTube")]/following-sibling::a'
    )
    social_media_telegram_xpath = (
        '//p/strong[contains(text(), "Telegram")]/following-sibling::a'
    )
    social_media_twitter_xpath = (
        '//p/strong[contains(text(), "Twitter")]/following-sibling::a'
    )
    social_media_facebook_xpath = (
        '//p/strong[contains(text(), "Facebook")]/following-sibling::a'
    )
    social_media_instagram_xpath = (
        '//p/strong[contains(text(), "Instagram")]/following-sibling::a'
    )
    social_media_linkedin_xpath = (
        '//p/strong[contains(text(), "LinkedIn")]/following-sibling::a'
    )


class PythonArgScrapperService:
    def __init__(
        self,
        gateway: Union[ChromeGateway, ChromeHubGateway],
        web_enums: PythonArgWebEnums,
    ) -> None:
        self._gateway = gateway
        self._enums = web_enums

    def get_python_argentina_social_links(self) -> Dict:
        self._gateway.get(url=self._enums.social_media_url.value)
        youtube = self._gateway.find_element_by_xpath(
            xpath=self._enums.social_media_youtube_xpath.value
        ).text
        telegram = self._gateway.find_element_by_xpath(
            xpath=self._enums.social_media_telegram_xpath.value
        ).text
        twitter = self._gateway.find_element_by_xpath(
            xpath=self._enums.social_media_twitter_xpath.value
        ).text
        facebook = self._gateway.find_element_by_xpath(
            xpath=self._enums.social_media_facebook_xpath.value
        ).text
        instagram = self._gateway.find_element_by_xpath(
            xpath=self._enums.social_media_instagram_xpath.value
        ).text
        linkedin = self._gateway.find_element_by_xpath(
            xpath=self._enums.social_media_linkedin_xpath.value
        ).text
        return dict(
            youtube=youtube,
            telegram=telegram,
            twitter=twitter,
            facebook=facebook,
            instagram=instagram,
            linkedin=linkedin,
        )


if __name__ == "__main__":
    scrapper = PythonArgScrapperService(
        gateway=ChromeGateway(), web_enums=PythonArgWebEnums
    )
    media_links = scrapper.get_python_argentina_social_links()
    pp = PrettyPrinter(indent=4)
    pp.pprint(media_links)
