from pprint import PrettyPrinter

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

if __name__ == "__main__":
    options = webdriver.ChromeOptions()
    options.add_argument("--disable-extensions")
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)

    driver.get(url="https://wiki.python.org.ar/foro_y_redes/")

    # Get links
    youtube = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "YouTube")]/following-sibling::a'
    ).text
    telegram = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Telegram")]/following-sibling::a'
    ).text
    twitter = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Twitter")]/following-sibling::a'
    ).text
    facebook = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Facebook")]/following-sibling::a'
    ).text
    instagram = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "Instagram")]/following-sibling::a'
    ).text
    linkedin = driver.find_element(
        By.XPATH, '//p/strong[contains(text(), "LinkedIn")]/following-sibling::a'
    ).text

    links = dict(
        youtube=youtube,
        telegram=telegram,
        twitter=twitter,
        facebook=facebook,
        instagram=instagram,
        linkedin=linkedin,
    )

    pp = PrettyPrinter(indent=4)
    pp.pprint(links)
